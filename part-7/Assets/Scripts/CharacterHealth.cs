﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterHealth : MonoBehaviour, IDamageable
{
    [SerializeField]
    private int startingHealth = 100;

    private int currentHealth;

    void Start()
    {
        currentHealth = startingHealth;
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        if (currentHealth <= 0)
        {
            Die();
        }
    }

    public void TakeHit(int damage, Collision collision)
    {
        TakeDamage(damage);
    }

    private void Die()
    {
        Died?.Invoke();
        Destroy(gameObject);
    }

    public event Action Died;
}
