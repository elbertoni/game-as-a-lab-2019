﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyController : MonoBehaviour
{
    private NavMeshAgent agent;
    public Transform Target { get; private set; }

    [SerializeField]
    private float pathUpdateRate = 4;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        Target = GameObject.FindGameObjectWithTag("Player").transform;
        Target.GetComponent<CharacterHealth>().Died += OnPlayerDeath;
        StartCoroutine(UpdateDestination());
    }

    private void OnPlayerDeath()
    {
        Target = null;
    }

    private IEnumerator UpdateDestination()
    {
        WaitForSeconds wait = new WaitForSeconds(1 / pathUpdateRate);
        while (Target != null)
        {
            agent.SetDestination(Target.position);
            yield return wait;
        }
    }
}
